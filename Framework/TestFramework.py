import os
from AnyCAD.PyAnyView import IRenderControl

class TestCase:
    def __init__(self, category:str, name:str):
        self.Name = name
        self.Category = category
        
    def Run(self, renderView:IRenderControl):
        print(self.Name)

    def GetDataFileName(self, fileName:str):
         _RootPath = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
         return os.path.join(_RootPath, "data/" + fileName)
    

_TestCase = {}
class TestSuit:
    @staticmethod
    def Add(case):
        _TestCase[case.Name] = case
    @staticmethod
    def Run(name, renderView:IRenderControl):
        _TestCase[name].Run(renderView)
    @staticmethod
    def ListAll():
        items = []
        for key in _TestCase:
            items.append(key)
        return items