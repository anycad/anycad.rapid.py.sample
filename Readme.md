
AnyCAD Rapid Py示例代码

## 环境准备

### 1.1 安装 vc_resit 2022

在Windows下，AnyCAD Rapid SDK依赖Vistual C++ 运行时库，64位版本需要在客户机上安装vc_redist.x64.exe

微软官方下载地址：
- x64: [vc_redist.x64.exe](https://aka.ms/vs/17/release/vc_redist.x64.exe)

### 1.2 安装 Python 3.12

安装过程略。

### 1.3 安装 PyAnyCAD

AnyCAD直接支持从pipy.org安装，在命令行中执行如下命令：

```cmd
pip install PyAnyCAD
```

### 1.4 安装 PyAnyCADQt5

如果需要使用PyQt5创建复杂的界面，则需要安装PyAnyCADQt5

```cmd
pip install PyAnyCADQt5
```