
import Test
from Framework import TestFramework
from AnyCAD.PyAnyCore import GlobalInstance
from AnyCAD.PyAnyView import RenderWindow

GlobalInstance.Initialize()

def RunTest(caseName):
    window = RenderWindow(caseName, 800, 600)
    TestFramework.TestSuit.Run(caseName, window)
    window.Show()
    window.Destroy()

Test.Initialize()

items = TestFramework.TestSuit.ListAll()
for i in range(len(items)):
    print("{0}: {1}".format(i, items[i]))


while(True):
    user_input = input("请输入序号: ")
    try:  
        number = int(user_input)  
        RunTest(items[number])
    except ValueError:  
        break

GlobalInstance.Destroy()