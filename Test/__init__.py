
import os,sys

def Initialize():
    sys.path.append(os.path.dirname(__file__))
    for dirpath, dirnames, filenames in os.walk(os.path.dirname(__file__)):
        for dirname in dirnames:            
            if(dirname.startswith("__")):
                continue
            #print(dirname)
            __import__(dirname)