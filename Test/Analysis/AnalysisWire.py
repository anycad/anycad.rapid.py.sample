from AnyCAD import PyAnyCAD as AnyCAD
from AnyCAD.PyAnyView import IRenderControl
from Framework import TestFramework

class TestAnalysisWire(TestFramework.TestCase):
    def __init__(self):
        super().__init__("Analysis","Wire")

    def Run(self, renderView:IRenderControl):
        fileName = self.GetDataFileName("path.brep")
        wire = AnyCAD.ShapeIO.Open(fileName)

        pc = AnyCAD.ParametricCurve(wire)
        start = pc.ComputePointByDistance(pc.FirstParameter(), 1)
        end = pc.ComputePointByDistance(pc.LastParameter(), -1)
        us = pc.SplitByUniformLength(1, start, end, 0.1)

        points = AnyCAD.ParticleSceneNode(len(us), AnyCAD.Vector3(1,0,0), 5)
        ii = 0
        for u in us:
            pt = pc.Value(u)
            points.SetPosition(ii, AnyCAD.Vector3(pt.X(), pt.Y(), pt.Z()))
            ii += 1

        points.ComputeBoundingBox(None)

        # 显示几何对象
        renderView.ShowSceneNode(points)
        renderView.ShowTopoShape(wire, None)

TestFramework.TestSuit.Add(TestAnalysisWire())