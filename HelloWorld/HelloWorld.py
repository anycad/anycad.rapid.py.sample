

from AnyCAD.PyAnyCore import GlobalInstance
from AnyCAD.PyAnyView import RenderWindow
from AnyCAD import PyAnyCAD as AnyCAD

GlobalInstance.Initialize()

window = RenderWindow("Hello AnyCAD", 800, 600)

box = AnyCAD.ShapeBuilder.MakeBox(AnyCAD.GAx2(), 1,1,1)
window.ShowTopoShape(box, None)

window.Show()

window.Destroy()
GlobalInstance.Destroy()



