import sys, os
from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5.QtCore import Qt

from AnyCAD.PyAnyCore import GlobalInstance
from AnyCADQt5.PyAnyQt5 import QtRenderControl
from AnyCAD import PyAnyCAD as AnyCAD

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("AnyCAD PyQt")
        self.setMinimumSize(1024, 768)
        
        self.renderView = QtRenderControl(self)
        self.setCentralWidget(self.renderView)

        box = AnyCAD.ShapeBuilder.MakeBox(AnyCAD.GAx2(), 1,1,1)
        self.renderView.ShowTopoShape(box, None)

if __name__ == '__main__':

    GlobalInstance.Initialize()
    GlobalInstance.SetDpiScaling(2)

    app = QApplication(sys.argv)
    
    w = MainWindow()
    w.show()
    app.exec()
    GlobalInstance.Destroy()
